# -*- coding: utf-8 -*-
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from PageObject.base import Base
from Locators.locators import *


class MenuPage(Base):

    """ Page object class for header Menu """
    
    def wait_for_menu_to_load(self):
        self.wait_page_to_load(MenuLocators.MENU_BTN)

    def click_on_menu_and_verify_expected_item_appeared(self, expected_item):
        self.click_element_and_wait(expected_item, *MenuLocators.MENU_BTN)

    def choose_clean_my_mac_and_verify_page_opened(self):
        self.click_element_and_wait(CleanMyMacPageLocators.DOWNLOAD_BOTTOM_BTN, *MenuLocators.CLEAN_MY_MAC_BTN)


class CleanMyMacDownloadPage(Base):

    """ Page object class for Clean My Mac Download page """

    def scroll_to_bottom(self):
        self.js_scroll(*CleanMyMacPageLocators.DOWNLOAD_BOTTOM_BTN)
 
    def click_bottom_download_page_and_verify_popup_opened(self):
        self.click_element_and_wait(SubscribePopupPageLocators.SUBSCRIBE_POPUP_EMAIL, *CleanMyMacPageLocators.DOWNLOAD_BOTTOM_BTN)
        

class SubscribePopupPage(Base):

    """ Page object class for Subscribe Popup page """
    
    def fill_in_popup_form(self, email):
        self.find_element(*SubscribePopupPageLocators.SUBSCRIBE_POPUP_EMAIL).send_keys(email)

    def click_subscribe_and_verify_success_msg(self):
        self.click_element_and_wait(SubscribePopupPageLocators.SUBSCRIBE_POPUP_SUCCESS_H3,*SubscribePopupPageLocators.SUBSCRIBE_POPUP_BTN)


class NewsletterPage(Base):

    """ Page object for Newsletter page """

    def open_subscription_url(self, url):
        self.open_url_in_new_tab(url)

    def asser_subscription_succeed(self, expected_text_1, expected_text_2):
        self.wait_page_to_load(NewsletterPageLocators.NEWSLETTER_SOCIAL_HEADER)
        text_1 = self.get_element_text(*NewsletterPageLocators.NEWSLETTER_SOCIAL_HEADER)
        text_2 = self.get_element_text(*NewsletterPageLocators.NEWSLETTER_SOCIAL_TEXT)
        assert text_1 == expected_text_1
        assert text_2 == expected_text_2
