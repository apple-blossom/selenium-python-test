# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By


class MenuLocators(object):

    """Locators for menu"""

    MENU_BTN = (By.CSS_SELECTOR, ".humb-text")
    CLEAN_MY_MAC_BTN = (By.XPATH, "//ul[contains(@class, 'products-list')]//span[contains(text(),'CleanMyMac X')]")
  
class CleanMyMacPageLocators(object):

    """Locators for Clean My Mac Page"""

    CLEAN_MY_MAC_PAGE_HEADER = (By.CSS_SELECTOR, "//h1[contains(text(),'CleanMyMac X')]")
    DOWNLOAD_BOTTOM_BTN = (By.CSS_SELECTOR, ".download.-cleanmymac-x [data-qa=FreeDownloadBtn]")


class SubscribePopupPageLocators(object):

    """Locators for Subscribe Popup afted downloading"""

    SUBSCRIBE_POPUP_EMAIL = (By.CSS_SELECTOR, ".modal [name=email]")
    SUBSCRIBE_POPUP_BTN = (By.CSS_SELECTOR, ".modal [data-qa=subscribeBtn]")
    SUBSCRIBE_POPUP_SUCCESS_H3 = (By.XPATH, "//div[contains(@class, 'modal')]//p[contains(text(),'Thanks for signing up!')]")

class NewsletterPageLocators(object):

    """Locators for Newsletter page after email verification"""

    NEWSLETTER_SOCIAL_HEADER = (By.CSS_SELECTOR, ".subscribe-success.-visible h5")
    NEWSLETTER_SOCIAL_TEXT = (By.CSS_SELECTOR, ".subscribe-success.-visible p")
