# -*- coding: utf-8 -*-
import json
import requests
from bs4 import BeautifulSoup

class Api_Helper(object):

	""" Api Helper """
	
	def get_mail_via_api(self, mail_url, key):
		response = requests.get(mail_url, headers={"X-RapidAPI-Host":"privatix-temp-mail-v1.p.rapidapi.com", "X-RapidAPI-Key": key})
		if response.status_code != 200:
		   raise Exception("Wrong status code:", response.status_code)
		try:
			email_json = json.loads(response.text)[0]
		except Exception as e:
		    raise e("Invalid response:" + response.text)
		else:
		    return email_json

	def verify_email_subject(self, content, subject_text):
		if content["mail_subject"] != subject_text:
			raise Exception(f"Wrong email subject. Expected {subject_text}, got " + content["mail_subject"])

	def get_url_from_email(self, content):
		parsed = BeautifulSoup(content["mail_text_only"], 'lxml')
		tag_a = parsed.find('a', {"class": "btn"})
		subscription_url = tag_a.get('href').split(r'\"')
		return str(subscription_url[0])
