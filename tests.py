import pytest

from Helpers.api_helper import Api_Helper
from PageObject.base import Base
from PageObject.pages import *
from Locators.locators import *


class TestTask():

    def setup(self):
        self.data = Base.open_data_json()
        self.driver = Base.setup_with_headless(self.data["CHROME_PATH"])
        self.menu_page = MenuPage(self.driver)
        self.clean_my_mac_page = CleanMyMacDownloadPage(self.driver)
        self.subscribe_popup_page = SubscribePopupPage(self.driver)
        self.newsletter_page = NewsletterPage(self.driver)
        self.api_helper = Api_Helper()
        url = self.data["URL"]
        self.user_email = self.data["EMAIL"]
        self.mail_url = self.data["EMAILURLMD5"]
        self.key = self.data["RAPID_API_KEY"]
        self.driver.get(url)

    def teardown(self):
        self.driver.quit()

    def test_download_app_and_sign_in_via_email(self):
        self.menu_page.wait_for_menu_to_load()
        self.menu_page.click_on_menu_and_verify_expected_item_appeared(MenuLocators.CLEAN_MY_MAC_BTN)
        self.menu_page.choose_clean_my_mac_and_verify_page_opened()
        self.clean_my_mac_page.scroll_to_bottom()
        self.clean_my_mac_page.click_bottom_download_page_and_verify_popup_opened()
        self.subscribe_popup_page.fill_in_popup_form(self.user_email)
        self.subscribe_popup_page.click_subscribe_and_verify_success_msg()
        mail = self.api_helper.get_mail_via_api(self.mail_url, self.key)
        self.api_helper.verify_email_subject(mail, "Take a second to confirm your subscription")
        subscription_url = self.api_helper.get_url_from_email(mail)
        self.newsletter_page.open_subscription_url(subscription_url)
        self.newsletter_page.asser_subscription_succeed("Thanks for signing up!", "Let’s get social. We’re always on.")

